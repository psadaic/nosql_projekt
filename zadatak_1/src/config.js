import path from 'path'
import appRoot from 'app-root-path'

const MONGO_URL      = 'mongodb://localhost:27017'
const MONGO_DATABASE = 'nosql_projekt_zad_1'
const DATASET_NAME   = 'parkinsons_updrs'
const DATASET_PATH   = path.resolve(appRoot.path, '../data/parkinsons_updrs.data')

export { MONGO_URL, MONGO_DATABASE, DATASET_NAME, DATASET_PATH }
