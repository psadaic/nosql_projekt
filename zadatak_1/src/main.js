import csv from 'csvtojson'
import mongo from 'mongodb'

import { MONGO_URL, MONGO_DATABASE, DATASET_NAME, DATASET_PATH } from "./config"

const VARIABLE_TYPE = {
  CONTINUOUS: 'continuous',
  CATEGORICAL: 'categorical',
}

const VARIABLES = {
  'subject#': VARIABLE_TYPE.CATEGORICAL,
  'age': VARIABLE_TYPE.CATEGORICAL,
  'sex': VARIABLE_TYPE.CATEGORICAL,
  'test_time': VARIABLE_TYPE.CONTINUOUS,
  'motor_UPDRS': VARIABLE_TYPE.CONTINUOUS,
  'total_UPDRS': VARIABLE_TYPE.CONTINUOUS,
  'Jitter(%)': VARIABLE_TYPE.CONTINUOUS,
  'Jitter(Abs)': VARIABLE_TYPE.CONTINUOUS,
  'Jitter:RAP': VARIABLE_TYPE.CONTINUOUS,
  'Jitter:PPQ5': VARIABLE_TYPE.CONTINUOUS,
  'Jitter:DDP': VARIABLE_TYPE.CONTINUOUS,
  'Shimmer': VARIABLE_TYPE.CONTINUOUS,
  'Shimmer(dB)': VARIABLE_TYPE.CONTINUOUS,
  'Shimmer:APQ3': VARIABLE_TYPE.CONTINUOUS,
  'Shimmer:APQ5': VARIABLE_TYPE.CONTINUOUS,
  'Shimmer:APQ11': VARIABLE_TYPE.CONTINUOUS,
  'Shimmer:DDA': VARIABLE_TYPE.CONTINUOUS,
  'NHR': VARIABLE_TYPE.CONTINUOUS,
  'HNR': VARIABLE_TYPE.CONTINUOUS,
  'RPDE': VARIABLE_TYPE.CONTINUOUS,
  'DFA': VARIABLE_TYPE.CONTINUOUS,
  'PPE': VARIABLE_TYPE.CONTINUOUS
}

/**
 * @param type
 * @returns {string[]}
 */
function variablesByType(type) {
  return Object.keys(VARIABLES).filter(variableKey => VARIABLES[variableKey] === type)
}

const CONTINUOUS_VARIABLES = variablesByType(VARIABLE_TYPE.CONTINUOUS)
const CATEGORICAL_VARIABLES = variablesByType(VARIABLE_TYPE.CATEGORICAL)

class Analyzer {
  constructor() {
    // Automatically run module on initialization.
    this.run()
  }

  async setupMongo() {
    // Build mongo client connection.
    this.mongoClient = await mongo.MongoClient
      .connect(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true })
      .catch(error => {
        throw new Error(`Unable to open mongo client connection: ${error}`)
      })
    // Open database.
    this.database = this.mongoClient.db(MONGO_DATABASE)
    // Clear old data if needed.
    await this.database.dropDatabase()
    // Open default collection.
    this.defaultCollection = this.database.collection(DATASET_NAME)

    console.log('Mongo setup complete.')
  }

  async readAndImportDataset() {
    // Read dataset file and insert to database.
    await csv({checkType: true})
      .fromFile(DATASET_PATH)
      .then((data) => {
        this.defaultCollection.insertMany(data)
      }, (reason) => {
        throw new Error(`Unable to read dataset file: ${reason}`)
      })

    console.log(`Dataset read and data imported successfully to ${DATASET_NAME} collection.`)
  }

  /**
   * 1. Sve nedostajuće vrijednosti kontinuirane varijable zamijeniti sa -1, a kategoričke sa „empty“.
   */
  async replaceMissingValues() {
    for(const VARIABLE in VARIABLES) {
      // Set and validate value based on attribute type.
      let value
      if(CONTINUOUS_VARIABLES.includes(VARIABLE)) value = -1
      else if(CATEGORICAL_VARIABLES.includes(VARIABLE)) value = 'empty'
      if(!value) continue
      // Update missing attribute value.
      await this.defaultCollection.updateMany(
        {
          $or: [
              // Match non-existing or null.
              { [VARIABLE]: { $exists: false } },
              { [VARIABLE]: { $type: 10 } }
            ]
          },
        {
          $set: {
            [VARIABLE]: value
          }
        }
      ).catch(error => {
        throw new Error(`Unable to replace missing value: ${error}`)
      })
    }

    console.log('[1] Missing values replaced.')
  }

  /**
   * 2. Za svaku kontinuiranu vrijednost izračunati srednju vrijednost, standardnu devijaciju i kreirati novi
   *    dokument oblika sa vrijednostima, dokument nazvati:  statistika_ {ime vašeg data seta}. U izračun se
   *    uzimaju samo nomissing  vrijednosti.
   */
  async calculateStatistics() {
    for(const VARIABLE of CONTINUOUS_VARIABLES) {
      // Calculate averages and standard deviations for nomissing variables.
      const data = await this.defaultCollection.aggregate([
        {
          $match: {
            [VARIABLE]: { $gt: -1 }
          }
        },
        {
          $group: {
            _id: VARIABLE,
            avg: { $avg: `$${VARIABLE}` },
            sd: { $stdDevPop: `$${VARIABLE}` },
            nomissing: { $sum: 1 }
          }
        }
      ]).toArray()
      // Store calculated values to statistika_* collection.
      await this.database.collection(`statistika_${DATASET_NAME}`).updateOne(
        { _id: VARIABLE },
        { $set: data[0] },
        { upsert: true }
      ).catch(error => {
        throw new Error(`Unable to store statistics calculation: ${error}`)
      })
    }

    console.log(`[2] Statistics calculated for continuous variables and stored inside statistika_${DATASET_NAME} collection.`)
  }

  /**
   * 3. Za svaku kategoričku  vrijednost izračunati frekvencije pojavnosti po obilježjima varijabli i kreirati
   *    novi dokument koristeći nizove,  dokument nazvati:  frekvencija_ {ime vašeg data seta} . Frekvencije računati
   *    koristeći $inc modifikator.
   */
  async calculateFrequencies() {
    for(const VARIABLE of CATEGORICAL_VARIABLES) {
      // Calculate variable frequencies.
      const data = await this.defaultCollection.aggregate([
        { $unwind: `$${VARIABLE}` },
        {
          $group: {
            _id: `$${VARIABLE}`,
            frequency: { $sum: 1 },
          }
        }
      ]).toArray()

      const document = {
        _id: VARIABLE,
        frequencies: {}
      }
      // Create value-key pairs for variable frequencies.
      for(const value of data) {
        document.frequencies[value._id] = value.frequency
      }
      // Store calculated values to frekvencija_* collection.
      await this.database.collection(`frekvencija_${DATASET_NAME}`).updateOne(
        { _id: VARIABLE },
        { $set: document },
        { upsert: true }
      ).catch(error => {
        throw new Error(`Unable to store frequencies calculation: ${error}`)
      })
    }

    console.log(`[3] Frequencies calculated for categorical variables and stored to frekvencija_${DATASET_NAME} collection.`)
  }

  /**
   * 4. Iz osnovnog  dokumenta kreirati dva nova dokumenta sa kontinuiranim vrijednostima u kojoj će u prvom
   *    dokumentu   biti sadržani svi elementi <= srednje vrijednosti , a u drugom dokumentu biti sadržani svi
   *    elementi >srednje vrijednosti , dokument nazvati:  statistika1_ {ime vašeg data seta} i
   *    statistika2_ {ime vašeg data seta}.
   */
  async splitByAverages() {
    for(const VARIABLE of CONTINUOUS_VARIABLES) {
      // Fetch variable average.
      const variableStats = await this.database.collection(`statistika_${DATASET_NAME}`).findOne({ _id: VARIABLE })
      const variableAvg = variableStats.avg
      // Find all values less or equal than average.
      const lteElements = await this.defaultCollection.aggregate([
        {
          $match: {
            [VARIABLE]: { $lte: variableAvg }
          }
        },
        {
          $group: {
            _id: null,
            values: { $push: `$${VARIABLE}` },
          }
        }
      ]).toArray()
      // Store values to statistika1_* collection.
      await this.database.collection(`statistika1_${DATASET_NAME}`).updateOne(
        { _id: VARIABLE },
        {
          $set: {
            _id: VARIABLE,
            values: lteElements[0].values
          }
        },
        { upsert: true }
      ).catch(error => {
        throw new Error(`Unable to store statistika1 values: ${error}`)
      })
      // Find all values greater than average.
      const gtElements = await this.defaultCollection.aggregate([
        {
          $match: {
            [VARIABLE]: { $gt: variableAvg }
          }
        },
        {
          $group: {
            _id: null,
            values: { $push: `$${VARIABLE}` },
          }
        }
      ]).toArray()
      // Store values to statistika2_* collection.
      await this.database.collection(`statistika2_${DATASET_NAME}`).updateOne(
        { _id: VARIABLE },
        {
          $set: {
            _id: VARIABLE,
            values: gtElements[0].values
          }
        },
        { upsert: true }
      ).catch(error => {
        throw new Error(`Unable to store statistika2 values: ${error}`)
      })
    }

    console.log(`[4.1] Variable elements with less or equal value than average stored to statistika1_${DATASET_NAME} collection.`)
    console.log(`[4.2] Variable elements with greater value than average stored to statistika2_${DATASET_NAME} collection.`)
  }

  /**
   * 5. Osnovni  dokument  kopirati u novi te embedati vrijednosti iz tablice 3 za svaku kategoričku vrijednost,
   *    emb_{ime vašeg data seta}.
   */
  async embedFrequencies() {
    // Fetch data for embedding.
    let embeddableDocuments = await this.defaultCollection.find({}).toArray()
    let embedWith = await this.database.collection(`frekvencija_${DATASET_NAME}`).find({}).toArray()

    for(let document of embeddableDocuments) {
      // Update original document with frequencies data.
      for(let data of embedWith) {
        document[data._id] = data.frequencies
      }
      // Store values to emb_* collection.
      await this.database.collection(`emb_${DATASET_NAME}`).updateOne(
        { _id: document._id },
        { $set: document },
        { upsert: true }
      ).catch(error => {
        throw new Error(`Unable to embed frequencies document: ${error}`)
      })
    }

    console.log(`[5] Categorical variables values replaced with frequencies and stored to emb_${DATASET_NAME} collection.`)
  }

  /**
   * 6. Osnovni  dokument  kopirati u novi te embedati vrijednosti iz tablice 2 za svaku kontinuiranu  vrijednost
   *    kao niz: emb2_ {ime vašeg data seta}.
   */
  async embedStatistics() {
    // Fetch data for embedding.
    const embeddableDocuments = await this.defaultCollection.find({}).toArray()
    const embedWith = await this.database.collection(`statistika_${DATASET_NAME}`).find({}).toArray()

    for(let document of embeddableDocuments) {
      // Update original document with frequencies data.
      for(let data of embedWith) {
        document[data._id] = {
          avg: data.avg,
          nomissing: data.nomissing,
          sd: data.sd
        }
      }
      // Store values to emb2_* collection.
      await this.database.collection(`emb2_${DATASET_NAME}`).updateOne(
        { _id: document._id },
        { $set: document },
        { upsert: true }
      ).catch(error => {
        throw new Error(`Unable to embed statistics document: ${error}`)
      })
    }

    console.log(`[6] Continuous variables values replaced with statistics and stored to emb2_${DATASET_NAME} collection.`)
  }

  /**
   * 7. Iz tablice emb2 izvući sve one srednje vrijednosti  iz  nizova čija je standardna devijacija 10% > srednje
   *    vrijednosti koristeći $set modifikator.
   */
  async popHigherThanAverageDeviations() {
    // Fetch documents.
    const documents = await this.database.collection(`emb2_${DATASET_NAME}`).find({}).toArray()

    for(const document of documents) {
      for(const VARIABLE of CONTINUOUS_VARIABLES) {
        // Fetch statistical variables.
        const avg = document[VARIABLE].avg;
        const nomissing = document[VARIABLE].nomissing;
        const sd = document[VARIABLE].sd;
        // Validate if standard deviation is >10% of average value.
        if(sd > (avg * 1.1)) {
          // Store values to emb2_* collection.
          await this.database.collection(`emb2_${DATASET_NAME}`).updateOne(
            { _id: document._id },
            {
              $set: {
                [VARIABLE]: {
                  nomissing: nomissing,
                  sd: sd
                }
              }
            }
          ).catch(error => {
            throw new Error(`Unable to pop higher than average deviation value document: ${error}`)
          })
        }
      }
    }

    console.log(`[7] Values with deviation of >10% than average successfully extracted from emb2_${DATASET_NAME} collection.`)
  }

  /**
   * 8. Kreirati složeni indeks na originalnoj tablici i osmisliti upit koji je kompatibilan sa indeksom.
   */
  async createAndQueryCompoundIndex() {
    // Create compound index.
    await this.defaultCollection.createIndex({
      'test_time': -1,
      'motor_UPDRS': 1,
      'total_UPDRS': 1
    }, { name: 'compound_index' }).catch(error => {
      throw new Error(`Unable to create compound index: ${error}`)
    })
    // Build query utilizing just created compound index.
    const result = await this.defaultCollection.find({
      'age': 72,
      'sex': 0
    }).sort({
      'test_time': -1,
      'motor_UPDRS': 1,
      'total_UPDRS': 1
    }).toArray()

    // Take only first 20 elements and only important fields to offload logging.
    const displayableResult = await result.map((object) => {
      return {
        age: object.age,
        sex: object.sex,
        'test_time (DESC)': object.test_time,
        'motor_UPDRS (ASC)': object.motor_UPDRS,
        'total_UPDRS (ASC)': object.total_UPDRS
      }
    }).slice(0, 20)

    console.log(`[8] Compound index created successfully. Query results preview:`)
    console.table(displayableResult)
  }

  async run() {
    try {
      await this.setupMongo()
      await this.readAndImportDataset()
      await this.replaceMissingValues()
      await this.calculateStatistics()
      await this.calculateFrequencies()
      await this.splitByAverages()
      await this.embedFrequencies()
      await this.embedStatistics()
      await this.popHigherThanAverageDeviations()
      await this.createAndQueryCompoundIndex()
      console.log('Done.')
    } catch (e) {
      console.error(e)
    } finally {
      await this.mongoClient.close()
    }
  }
}

const analyzer = new Analyzer()

export { analyzer }
