
# Zadatak 1

Prvi dio NoSQL projektnog zadatka.

---
## Requirements

Fairly recent version of Node.js, npm and MongoDB is required.

### Install dependencies

    $ npm install
    
### Running the project

    $ npm start
