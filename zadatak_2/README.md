
# Zadatak 2

Drugi dio NoSQL projektnog zadatka.

---
## Requirements

Anaconda 3 (JupyterLab, conda, notebook, ...)

### Running the project

    $ jupyter-notebook --notebook-dir=../
    $ Open zadatak_2/zadatak_2.ipynb notebook.
